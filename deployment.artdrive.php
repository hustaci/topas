<?php

return array(
	'topol.drive.cz' => array(
		'remote' => 'ftp://topoldrive:ibp8auAk7m@topol.drive.cz/www',
		'local' => '.',
		'repository' => 'https://vojtatranta:1992Vojtechtranta@bitbucket.org/vojtatranta/topas/get/master.zip',
		'zip_storage' => '../repos',
		'test' => FALSE,
		'ignore' => '
			.git*
			project.pp[jx]
			/deployment.*
			/log
			temp/*
			!temp/.htaccess
			*.deploytmp
			sites/all/themes/topas/node_modules
			sites/all/themes/topas/src/bower_components
		',
		'allowdelete' => TRUE,
		'before' => array(
			
		),
		'after' => array(
		),
		'purge' => array(
			'temp/cache',
		),
		'preprocess' => FALSE,
		'tempdir' => __DIR__ . '/temp',
		'colors' => TRUE,
	),
);