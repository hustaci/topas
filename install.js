// For all available options, see node_modules/pho-devstack/config.js
// These are development build settings, see gulpfile-production.js for production settings

var fs = require('fs');
var readline = require('readline');

var rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout
});



var hostsPaths = {
  win: 'C:\\Windows\\System32\\drivers\\etc\\hosts',
  osx: '/etc/hosts',
};

var virtualHostsPaths = {
  mamp: '/Applications/MAMP/conf/apache/extra/httpd-vhosts.conf',
  wamp: 'C:\\wamp\\bin\\apache\\apache2.4.9\\conf\\extra\\httpd-vhosts.conf',

};

var webName = 'newsite.loc';




var webDir = __dirname;
var os = 'win';

var server = 'wamp';

rl.question("Type domain of web (eg. mysite.loc): ", function(answer) {

  if (answer.length > 0) webName = answer;


  rl.question("Whats your OS? Type 'win' for windows and 'osx' for Mac: ", function(answer)
  {
    if (answer.length > 0) os = answer;


    rl.question("Whats your Server? Type 'wamp' for WAMP and 'mamp' for MAMP: ", function(answer)
    {
      if (answer.length > 0) server = answer;

        if (typeof webName == 'undefined') return console.error('You must specify web local url in substitue-config.js eg. mylocalweb.loc');
    
        var hostsStr = "\n127.0.0.1\t" + webName;

        fs.appendFile(hostsPaths[os], hostsStr, function(er, fl)
        {
          //console.log(er, fl);
        });



        var vhostStr = "\n\n <VirtualHost *:80> \n \t\tDocumentRoot '" + __dirname + "'\n\t\tServerName " + webName + "\n\t\tServerAlias www." + webName + "\n<Directory '"+ __dirname +"'>\nOptions FollowSymLinks\nAllowOverride All\n\nOrder allow,deny\nAllow from all\n</Directory>\n</VirtualHost>";

        fs.appendFile(virtualHostsPaths[server], vhostStr, function(er, fl)
        {
          //console.log(er, fl);
        });

        rl.close();
    });

  });


});


